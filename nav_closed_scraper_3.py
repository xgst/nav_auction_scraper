#!usr/bin/python3

import os
import re
import time
import datetime
import unicodedata
from pathlib import Path

import numpy as np
import pandas as pd
import requests
from bs4 import BeautifulSoup

def logger(log_file, text):
    with open(log_file, "a") as logfile:
        logfile.write(text)
        logfile.write("\n")

DETAIL_DIR = Path("/home/NAV_auctions/detail")
LOG_FILE = Path("/home/NAV_auctions/logs/nav_log.txt")

ts =  datetime.datetime.now().strftime("%Y_%m_%d")
print(ts)
logger(LOG_FILE, "")
logger(LOG_FILE, "agent : Closed auctions scraper")
logger(LOG_FILE, f"started: {datetime.datetime.now().strftime('%Y %m %d %H %M')}")

files = os.listdir(DETAIL_DIR)
paths = [os.path.join(DETAIL_DIR,file) for file in files]
latest_file = max(paths,key=os.path.getctime)
logger(LOG_FILE,f"latest_file: {latest_file}")

collector_df = pd.read_csv(DETAIL_DIR/latest_file)
collector_df.drop_duplicates(subset=["auction_id"],inplace=True)

logger(LOG_FILE,f"base_len:{len(collector_df)}")

collector_df.loc[:,"auction_ends"] = pd.to_datetime(collector_df.loc[:,"auction_ends"],
                                                    infer_datetime_format=True)

collector_df.set_index("auction_id", inplace=True)

today = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

ended_auctions_df = collector_df[collector_df.loc[:,"auction_ends"] < today]

not_collected_ended_auctions = ended_auctions_df[ended_auctions_df["sold"].isnull()]

print(len(not_collected_ended_auctions))
logger(LOG_FILE,f"not_collected : {len(not_collected_ended_auctions)}")

if len(not_collected_ended_auctions) > 0:

    ended_auction_ids = not_collected_ended_auctions.index

    ended_df = pd.DataFrame()

    for auction_id in tqdm(ended_auction_ids):
        url = "http://arveres.nav.gov.hu/index-arveres_osszesites-ingosag."\
            "html?.actionId=action.auction.AddParticipantAction&"\
            f"auctionId={auction_id}&FRAME_SKIP_DEJAVU=1"
        res = requests.get(url)
        time.sleep(2)
        res.raise_for_status()
        content = res.text
        soup = BeautifulSoup(content, 'html.parser')
        if "törölve" in soup.get_text():
            row_dict = {}
            row_dict["auction_id"] = [auction_id]
            row_dict["final_price"] = [np.NaN]
            row_dict["last_bid_time"] = [np.NaN]
            row_dict["bidder"] = ["deleted"]
            row_dict["robot_bid"] = [np.NaN]
            row_dict["bidder_status"] = [np.NaN]
            row_dict["auction_url"] = [url]
            row_dict["sold"] = [False]
            row_df = pd.DataFrame.from_dict(row_dict,orient="columns")

        else:
            final_price = None
            try:
                final_price = re.findall('[0-9]+',
                                         soup.find_all("font")[1].get_text())
            except IndexError as e:
                final_price = None
            pass

            if (final_price is not None
                and final_price != []
                and final_price != ['0']):

                rows = soup.find_all("tr",{'class': "Bg2"})
                row_dict = {}
                row_dict["auction_id"] = [auction_id]

                try:
                    auctioner_details = rows[2].find_all("td")
                    row_dict["final_price"] = [(int(unicodedata.normalize('NFKC',
                                               auctioner_details[0]
                                               .get_text()).replace(" ","")))]
                    row_dict["last_bid_time"] = [auctioner_details[1].get_text()]
                    row_dict["bidder"] = [auctioner_details[2].get_text()]
                    row_dict["robot_bid"] = [auctioner_details[3].get_text()]
                    row_dict["bidder_status"] = [auctioner_details[4].get_text()]
                    row_dict["auction_url"] = [url]
                    row_dict["sold"] = [True]
                except ValueError as e:
                    auctioner_details = rows[3].find_all("td")
                    row_dict["final_price"] = [(int(unicodedata.normalize('NFKC',
                                               auctioner_details[0]
                                               .get_text()).replace(" ","")))]
                    row_dict["last_bid_time"] = [auctioner_details[1].get_text()]
                    row_dict["bidder"] = [auctioner_details[2].get_text()]
                    row_dict["robot_bid"] = [auctioner_details[3].get_text()]
                    row_dict["bidder_status"] = [auctioner_details[4].get_text()]
                    row_dict["auction_url"] = [url]
                    row_dict["sold"] = [True]

                row_df = pd.DataFrame.from_dict(row_dict,
                                                orient="columns")
            else:
                rows = soup.find_all("tr",{'class': "Bg2"})
                row_dict = {}
                row_dict["auction_id"] = [auction_id]
                row_dict["final_price"] = [np.NaN]
                row_dict["last_bid_time"] = [np.NaN]
                row_dict["bidder"] = [np.NaN]
                row_dict["robot_bid"] = [np.NaN]
                row_dict["bidder_status"] = [np.NaN]
                row_dict["auction_url"] = [url]
                row_dict["sold"] = [False]
                row_df = pd.DataFrame.from_dict(row_dict,
                                                orient="columns")

        ended_df = ended_df.append(row_df,
                                       ignore_index=True)

    backup_df = ended_df.copy()
    ended_df["auction_id"] = ended_df["auction_id"].astype("str")
    ended_df.dropna(subset=["auction_id"], inplace=True)
    ended_df.drop_duplicates(subset=["auction_id"], inplace=True)
    ended_df.set_index(ended_df["auction_id"], drop=True, inplace=True)

    collector_df.update(ended_df) #default is inplace

    #collector_df["final_per_est_price"] = collector_df["final_price"] / collector_df["est_value"]

    collector_df.to_csv(DETAIL_DIR/f"reszletes_nav_arveresek_{ts}.csv")
    print(DETAIL_DIR/f"reszletes_nav_arveresek_{ts}.csv")
    logger(LOG_FILE,f"saved_to: {DETAIL_DIR}/reszletes_nav_arveresek_{ts}.csv")
    logger(LOG_FILE,"end_status: run successfully")

else:
    print("no new auction endings")
    logger(LOG_FILE,"end_status: no new ending")
