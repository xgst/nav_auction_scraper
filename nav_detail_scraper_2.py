import re
import os
import time
import datetime
import unicodedata
from pathlib import Path

import pandas as pd
import requests
from bs4 import BeautifulSoup

BASE_DIR = Path("/home/NAV_auctions/base")
DETAIL_DIR = Path("/home/NAV_auctions/detail")

files = os.listdir(DETAIL_DIR)
paths = [os.path.join(DETAIL_DIR,file)for file in files]
latest_file = max(paths,key=os.path.getctime)
print(latest_file)

if "lock" in latest_file:
    print("aha, ott hagyott egy lock filet a loffice")
print(latest_file)

prev_df = pd.read_csv(DETAIL_DIR/latest_file)
print(len(prev_df))

ts =  datetime.datetime.now().strftime("%Y_%m_%d")
today_df = pd.read_csv(BASE_DIR/f"nav_arveresek_{ts}.csv")

prev_df["posted_on"] = pd.to_datetime(prev_df["posted_on"],
                                      infer_datetime_format=True)
today_df["posted_on"] = pd.to_datetime(today_df["posted_on"],
                                       infer_datetime_format=True)

prev_max_date = prev_df["posted_on"].max()
today_max_date = today_df["posted_on"].max()

print(f"previous report posting date: {prev_max_date}")
print(f"fresh report posting date: {today_max_date}")
    
if today_max_date > prev_max_date:
    new_items_df = today_df[(today_df["posted_on"] <= today_max_date) 
         & (today_df["posted_on"] > prev_max_date)]
    
    print(len(new_items_df))
    collector_df = pd.DataFrame()
    
    for url in new_items_df["item_url"][:]:
        time.sleep(2)
        res = requests.get(url)
        res.raise_for_status()
        content = res.text
        soup = BeautifulSoup(content, 'html.parser')
        rows = soup.find_all("tr",{'class': "Bg2"})
        row_dict = {}

        for row in rows:
            row = row.get_text()
            row_dict["url"] = [url]
            if "Árverés megnevezése" in row:
                row_dict["auction_name"] = [row.split(":")[1].replace("\n", "")]
            if "Végrehajtási ügyszám" in row:
                row_dict["enforcement_id"] = [row.split(")")[1].replace("\n", "")]
            if "Árverés kategória" in row:
                row_dict["auction_cat"] = [row.replace("\n", "").split(":")[1]]
            if "Árverés kategória" in row:            
                row_dict["auction_cat"] = [row.replace("\n", "").split(":")[1]]
            if "Árverés sorszáma:" in row:
                row_dict["auction_id_long"] = [row.split(":")[1].replace("\n", "")]
            if "Adós ÁFA/EVA alany" in row:
                row_dict["debtor_VAT_entity"] = [row.split(":")[1].replace("\n", "")]
            if "Árverés meghirdetése:" in row:
                row_dict["posted_on"] = [row.split(":")[1].replace("\n", "")]
            if "Árverés kezdete:" in row:
                row_dict["auction_starts"] = [row.split(":")[1].replace("\n", "")]
            if "Árverés befejezése:" in row:
                row_dict["auction_ends"] = [row.split(":")[1].replace("\n", "")]
            if "Ügyintéző telefon:" in row:
                row_dict["admin_tel"] = [row.split(":")[1].replace("\n", "")]
            if "Ügyintéző email:" in row:
                row_dict["admin_mail"] = [row.split(":")[1].replace("\n", "")]
            if "Az árverezett tétel megtekinthető, hely:" in row:
                row_dict["auction_place"] = [row.split(":")[1].replace("\n", "")]
            if "Az árverezett tétel megtekinthető, idő:" in row:
                item_view_1 = row.split(":")[1]
                item_view_2 = unicodedata.normalize('NFKC', row.split(":")[2]) 
                row_dict["item_view"] = [":".join([item_view_1,item_view_2])]
            if "Tétel megnevezése:" in row:
                row_dict["item_name"] = [row.split(":")[1].replace("\n", "")]
            if "Becsérték:" in row:
                est_value_raw = row.split(":")[1].replace("\n", "").replace("\t", "")
                row_dict["est_value"] = ([unicodedata.normalize('NFKC', est_value_raw)
                                          .replace("HUF", "").replace(" ", "")])
            if "Minimál ajánlat:" in row:
                min_value_raw = row.split(":")[1].replace("\n", "").replace("\t", "")
                row_dict["min_value"] = ([unicodedata.normalize('NFKC', min_value_raw)
                                          .replace("HUF", "").replace(" ", "")])
            if "Egyszerre árverezett tétel darabszám:" in row:
                row_dict["item_count"] = ([int(row.split(":")[1].replace("\t", "")
                                               .replace("\n", "").replace("darab", ""))])
            if "Állapot:" in row:     
                row_dict["item_condition"] = [row.split(":")[1].replace("\n", "")]
            if "Egyéb infó:" in row:      
                row_dict["item_info"] = [row.split(":")[1].replace("\n", "")]
            if "Tétel ismertető word vagy PDF formátumban:" in row:
                row_dict["pdf_word_attachment"] = [row.split(":")[1].replace("\n", "")]

        row_df = pd.DataFrame.from_dict(row_dict, orient="columns")
        collector_df = collector_df.append(row_df, ignore_index=True)
        
    collector_df[["auction_id","auction_id_long"]] \
    = collector_df["auction_id_long"].str.split("/", expand=True)

    collector_df["postal_code"] = collector_df["auction_place"].str[:4]

    collector_df = prev_df.append(collector_df, ignore_index=True)
    
    collector_df.dropna(subset=["auction_id"], inplace=True)

    collector_df.set_index("auction_id", inplace=True)
    
    collector_df.to_csv(DETAIL_DIR/f"reszletes_nav_arveresek_{ts}.csv")

    print(DETAIL_DIR)
    print(f"reszletes_nav_arveresek{ts}.csv")
else:
    print("no new items")
    
    prev_df.dropna(subset=["auction_id"], inplace=True)
    prev_df["auction_id"] = prev_df["auction_id"].astype("int")
    prev_df.set_index("auction_id", nplace=True)
    prev_df.to_csv(DETAIL_DIR/f"reszletes_nav_arveresek{ts}.csv")